# Apptus Academy Environment
## Description
The project environment is implemented on the docker and docker-compose v3.

## Start
Run command for up docker environment
```sh
docker-compose up
```

Run command for rebuild environment 
```sh
docker-compose up --build
```